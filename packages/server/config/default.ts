import { join } from 'path';

export default {
	database: {
		type: 'mysql',
		port: 3306,
		host: 'localhost',
		username: 'root',
		password: '',
		databaseName: '',
		GAPI_ID: '',

		entities: [join(__dirname, '../src/entities/**{.ts,.js}')],
		migrations: [join(__dirname, '../src/migrations/**{.ts,.js}')],
		cli: {
			migrationsDir: join(__dirname, '../src/migrations'),
		},
	},
	server: {
		port: 5000,
	},
	origin: '',
};
