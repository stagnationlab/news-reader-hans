import config from './src/services/config';

module.exports = {
	type: config.database.type,
	host: config.database.host,
	port: config.database.port,
	username: config.database.username,
	password: config.database.password,
	database: config.database.databaseName,

	entities: ['src/entities/*.ts'],
	migrationsTableName: 'migrations',
	migrations: ['src/migrations/*.ts'],
	cli: {
		migrationsDir: 'src/migrations',
	},
};
