import { objectType } from 'nexus';

const Item = objectType({
	name: 'Item',
	definition(t) {
		t.string('title');
		t.string('description');
		t.string('url');
		t.string('img');
	},
});

export default Item;
