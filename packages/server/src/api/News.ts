import { stringArg, queryType } from 'nexus';
import { getConnection } from 'typeorm';
import NewsItemEntity from '../entities/NewsItemEntity';
import Item from './Item';

const News = queryType({
	definition(t) {
		t.list.field('allNews', {
			type: Item,
			resolve: async (parent, args, { req }) => {
				if (!req.body.user) {
					return [];
				}
				let connection = getConnection();

				if (!connection) {
					throw Error;
				}

				let news = await NewsItemEntity.find();

				let newsArray = [];

				news.map((item) => {
					newsArray.push({
						title: item.title,
						description: item.description,
						url: item.url,
						img: item.img,
					});
				});

				return newsArray;
			},
		});
	},
});

export default News;
