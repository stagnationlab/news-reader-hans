import { mutationType } from 'nexus';

const Login = mutationType({
	definition(t) {
		t.string('loginMutation', {
			resolve: async (parent, args, { req }) => {
				if (!req.body.user) {
					return '';
				}
				return req.body.user.name;
			},
		});
	},
});

export default Login;
