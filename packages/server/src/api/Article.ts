import { stringArg, extendType } from 'nexus';
import { getConnection } from 'typeorm';
import NewsItemEntity from '../entities/NewsItemEntity';
import Item from './Item';

const Article = extendType({
	type: 'Query',
	definition(t) {
		t.field('article', {
			type: Item,
			args: { newsTitle: stringArg() },
			resolve: async (parent, { newsTitle }, { req }) => {
				let connection = getConnection();

				if (!connection) {
					throw Error;
				}

				let article = { title: '', description: '', url: '', img: '' };

				let newsItem = await NewsItemEntity.findOne({ where: { title: newsTitle } });

				if (!newsItem) {
					return article;
				}

				article.title = newsItem.title;
				article.description = newsItem.description;
				article.url = newsItem.url;
				article.img = newsItem.img;

				return article;
			},
		});
	},
});

export default Article;
