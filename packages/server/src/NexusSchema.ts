import { makeSchema } from 'nexus';

import News from './api/News';
import Article from './api/Article';
import Item from './api/Item';
import Login from './api/Login';

const schema = makeSchema({
	types: [News, Login, Item, Article],
	outputs: {
		schema: __dirname + '/api/generated/schema.graphql',
		typegen: __dirname + '/api/generated/typings.ts',
	},
});

export default schema;
