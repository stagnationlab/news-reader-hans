import NewsItemEntity from '../entities/NewsItemEntity';
import axios from 'axios';
import { getConnection } from 'typeorm';

export default async () => {
	const connection = getConnection();

	if (!connection) {
		throw Error;
	}

	let newsItemEntity = await NewsItemEntity.find();
	if (newsItemEntity.length != 0) {
		await NewsItemEntity.query('TRUNCATE TABLE `newsItems`');
	}

	let response = await axios.get(
		'https://newsapi.org/v2/top-headlines?pageSize=100&category=sport&country=us&apiKey=ff514cc67af94794bf59d3a5839d3c60',
	);
	let articles = response.data.articles;

	articles.map(async (article) => {
		const newsItem = new NewsItemEntity();

		if (
			typeof article.title === 'string' &&
			typeof article.description === 'string' &&
			typeof article.url === 'string' &&
			typeof article.urlToImage === 'string'
		) {
			(newsItem.title = article.title),
				(newsItem.description = article.description),
				(newsItem.url = article.url),
				(newsItem.img = article.urlToImage);

			await newsItem.save();
		}
	});
};
