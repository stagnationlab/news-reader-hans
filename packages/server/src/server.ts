import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import cors from 'cors';
import { OAuth2Client } from 'google-auth-library';
import path from 'path';

import Schema from './NexusSchema';
import createDatabaseConnection from './services/createDatabaseConnection';
import UserEntity from './entities/UserEntity';
import config from './services/config';
import databaseScript from './scripts/updateDatabaseWithNews';

import cron from 'node-cron';

export async function start() {
	const app = express();
	app.use(express.static(path.join(__dirname, '../../', 'client/build')));

	const GAPI_ID = config.database.GAPI_ID;

	var corsOptions = {
		origin: [config.origin],
		credentials: true, // <-- REQUIRED backend setting
	};
	app.use(cors(corsOptions));

	const connection = await createDatabaseConnection();

	if (!connection) {
		throw Error;
	}

	cron.schedule('0 0 */1 * * *', () => {
		databaseScript();
	});

	// Makes /graphql route
	const server = new ApolloServer({
		schema: Schema,
		context: async ({ req, res }) => {
			const token = req.headers['authorization'];

			if (!token) {
				req.body.user = undefined;
				return { req, res };
			}

			let tokenValue = token.split(' ')[1];

			const client = new OAuth2Client(GAPI_ID);
			const ticket = await client.verifyIdToken({
				idToken: tokenValue,
				audience: GAPI_ID,
			});

			if (ticket) {
				const payload = ticket.getPayload();
				const account_googleID = payload['sub'];
				const account_name = payload['given_name'];

				let account = await UserEntity.findOne({
					where: { googleAccount: account_googleID },
				});

				if (!account) {
					account = new UserEntity();
					account.googleAccount = account_googleID;
					account.name = account_name;
					account.save();
				}

				req.body = { ...req.body, user: account };
			}

			return { req, res };
		},
	});

	server.applyMiddleware({
		app,
		path: `${server.graphqlPath}`,
	});

	const PORT = config.server.port || process.env.PORT;

	app.listen(PORT);
}

(() => {
	if (require.main !== module) {
		return;
	}

	start();
})();
