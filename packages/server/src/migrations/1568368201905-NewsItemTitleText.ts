import { MigrationInterface, QueryRunner } from 'typeorm';

export class NewsItemTitleText1568368201905 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE `newsItem` ADD `title` text NOT NULL');
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('DROP TABLE `newsItem`');
	}
}
