import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdatedNewsItemEntity1568018696816 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `newsItem` CHANGE `content` `description` varchar(255) NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `newsItem` CHANGE `description` `content` varchar(255) NOT NULL");
    }

}
