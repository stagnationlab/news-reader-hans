import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenamedNewsItemToPlural1568625971226 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('RENAME TABLE `newsItems` TO `newsItems`');
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('DROP TABLE `newsItems`');
	}
}
