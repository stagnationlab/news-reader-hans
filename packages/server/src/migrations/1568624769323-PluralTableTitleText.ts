import { MigrationInterface, QueryRunner } from 'typeorm';

export class PluralTableTitleText1568624769323 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE `newsItem` MODIFY `title` TEXT NOT NULL');
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('DROP TABLE `newsItems`');
	}
}
