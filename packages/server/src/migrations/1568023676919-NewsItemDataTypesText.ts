import {MigrationInterface, QueryRunner} from "typeorm";

export class NewsItemDataTypesText1568023676919 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `newsItem` DROP COLUMN `description`");
        await queryRunner.query("ALTER TABLE `newsItem` ADD `description` text NOT NULL");
        await queryRunner.query("ALTER TABLE `newsItem` DROP COLUMN `url`");
        await queryRunner.query("ALTER TABLE `newsItem` ADD `url` text NOT NULL");
        await queryRunner.query("ALTER TABLE `newsItem` DROP COLUMN `img`");
        await queryRunner.query("ALTER TABLE `newsItem` ADD `img` text NOT NULL");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `newsItem` DROP COLUMN `img`");
        await queryRunner.query("ALTER TABLE `newsItem` ADD `img` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `newsItem` DROP COLUMN `url`");
        await queryRunner.query("ALTER TABLE `newsItem` ADD `url` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `newsItem` DROP COLUMN `description`");
        await queryRunner.query("ALTER TABLE `newsItem` ADD `description` varchar(255) NOT NULL");
    }

}
