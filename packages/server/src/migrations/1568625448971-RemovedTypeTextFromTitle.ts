import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemovedTypeTextFromTitle1568625448971 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE `newsItem` MODIFY COLUMN `title` TEXT NOT NULL');
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.query('ALTER TABLE `newsItem` DROP COLUMN `title`');
		await queryRunner.query('ALTER TABLE `newsItem` ADD `title` text NOT NULL');
	}
}
