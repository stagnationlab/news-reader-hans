import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdatedSessionEntity1567071448842 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP INDEX `IDX_28c5d1d16da7908c97c9bc2f74` ON `session`");
        await queryRunner.query("ALTER TABLE `session` DROP COLUMN `expiredAt`");
        await queryRunner.query("ALTER TABLE `session` DROP COLUMN `json`");
        await queryRunner.query("ALTER TABLE `session` ADD `sessionId` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `session` ADD `expiresAt` int NOT NULL");
        await queryRunner.query("ALTER TABLE `session` ADD `data` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `session` DROP PRIMARY KEY");
        await queryRunner.query("ALTER TABLE `session` DROP COLUMN `id`");
        await queryRunner.query("ALTER TABLE `session` ADD `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `session` DROP COLUMN `id`");
        await queryRunner.query("ALTER TABLE `session` ADD `id` varchar(255) NOT NULL");
        await queryRunner.query("ALTER TABLE `session` ADD PRIMARY KEY (`id`)");
        await queryRunner.query("ALTER TABLE `session` DROP COLUMN `data`");
        await queryRunner.query("ALTER TABLE `session` DROP COLUMN `expiresAt`");
        await queryRunner.query("ALTER TABLE `session` DROP COLUMN `sessionId`");
        await queryRunner.query("ALTER TABLE `session` ADD `json` text NOT NULL");
        await queryRunner.query("ALTER TABLE `session` ADD `expiredAt` bigint NOT NULL");
        await queryRunner.query("CREATE INDEX `IDX_28c5d1d16da7908c97c9bc2f74` ON `session` (`expiredAt`)");
    }

}
