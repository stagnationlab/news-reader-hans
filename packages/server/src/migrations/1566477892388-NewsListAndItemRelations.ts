import {MigrationInterface, QueryRunner} from "typeorm";

export class NewsListAndItemRelations1566477892388 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `news` DROP COLUMN `newsList`");
        await queryRunner.query("ALTER TABLE `newsItem` ADD `newsListId` int NULL");
        await queryRunner.query("ALTER TABLE `newsItem` ADD CONSTRAINT `FK_3d1a7b991213d772193ae1c0b03` FOREIGN KEY (`newsListId`) REFERENCES `news`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `newsItem` DROP FOREIGN KEY `FK_3d1a7b991213d772193ae1c0b03`");
        await queryRunner.query("ALTER TABLE `newsItem` DROP COLUMN `newsListId`");
        await queryRunner.query("ALTER TABLE `news` ADD `newsList` varchar(255) NOT NULL");
    }

}
