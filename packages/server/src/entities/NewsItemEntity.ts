import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';

@Entity('newsItems')
export default class NewsItemEntity extends BaseEntity {
	@PrimaryGeneratedColumn()
	id: number;

	@Column({ type: 'text' })
	title: string;

	@Column({ type: 'text' })
	description: string;

	@Column({ type: 'text' })
	url: string;

	@Column({ type: 'text' })
	img: string;
}
