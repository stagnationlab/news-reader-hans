import config from 'config'; // from nod_mod
import path from 'path';

const ourConfigDir = path.join(__dirname, '..', '..', 'config');
let baseConfig = config.util.loadFileConfigs(ourConfigDir);

if (process.env.NODE_ENV === 'production') {
	baseConfig = baseConfig.default;
}

export default baseConfig;
