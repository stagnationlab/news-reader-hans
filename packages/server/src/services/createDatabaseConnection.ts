import { createConnection, ConnectionOptions } from 'typeorm';
import path from 'path';
import config from '../services/config';

const createDatabaseConnection = async () => {
	const options: ConnectionOptions = {
		type: config.database.type,
		host: config.database.host,
		port: config.database.port,
		username: config.database.username,
		password: config.database.password,
		database: config.database.databaseName,

		migrationsRun: false,
		entities: config.database.entities,
		migrationsTableName: 'migrations',
		migrations: config.database.migrations,
		cli: {
			migrationsDir: path.join(__dirname, '../migrations'),
		},
	};

	const conn = await createConnection(options);

	if (!conn) {
		throw Error;
	}
	console.log('Running migrations');
	conn.runMigrations();

	return conn;
};

export default createDatabaseConnection;
