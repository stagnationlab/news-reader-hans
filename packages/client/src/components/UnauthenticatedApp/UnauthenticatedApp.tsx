import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from '../Header/Header';
import Modal from '../Modal/Modal';
import LoginGoogle from '../GoogleButtons/LoginGoogle';

const UnauthenticatedApp = ({ setUser }) => {
	return (
		<BrowserRouter>
			<Header />
			<LoginGoogle setUser={setUser} />
			<Modal />
		</BrowserRouter>
	);
};

export default UnauthenticatedApp;
