import React, { useState, useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import AuthenticatedApp from '../AuthenticatedApp/AuthenticatedApp';
import UnauthenticatedApp from '../UnauthenticatedApp/UnauthenticatedApp';

const LOGIN_MUTATION = gql`
	mutation LogInMutation {
		loginMutation
	}
`;

const App = () => {
	const [loginMutation] = useMutation(LOGIN_MUTATION);
	const [user, setUser] = useState('');

	useEffect(() => {
		(async () => {
			let token = localStorage.getItem('token');

			if (!token) {
				setUser('');
				return;
			}

			const loginMutationResponse = await loginMutation().catch((err) => localStorage.removeItem('token'));

			if (!loginMutationResponse) {
				localStorage.removeItem('token');
				setUser('');
			} else {
				setUser(loginMutationResponse && loginMutationResponse.data.loginMutation);
			}
		})();
	}, [user]);

	return user ? <AuthenticatedApp user={user} setUser={setUser} /> : <UnauthenticatedApp setUser={setUser} />;
};

export default App;
