import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import NewsList from '../NewsList/NewsList';
import Preferences from '../Preferences/Preferences';
import Article from '../Article/Article';
import LogoutGoogle from '../GoogleButtons/LogoutGoogle';

const AuthenticatedApp = ({ user, setUser }) => {
	return (
		<BrowserRouter>
			<LogoutGoogle setUser={setUser} />
			<Switch>
				<Route exact path="/" render={(routeProps) => <NewsList {...routeProps} name={user} />} />
				<Route exact path="/preferences" component={Preferences} />
				<Route path="/:title" render={(routeProps) => <Article {...routeProps} name={user} />} />
			</Switch>
		</BrowserRouter>
	);
};

export default AuthenticatedApp;
