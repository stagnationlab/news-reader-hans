import React from 'react';
import { GoogleLogout } from 'react-google-login';
import styles from './googleButtons.module.scss';

const LogoutGoogle = ({ setUser }) => {
	const onGoogleLogoutSuccess = () => {
		localStorage.removeItem('token');

		setUser('');
	};

	return (
		<GoogleLogout
			clientId={process.env.REACT_APP_GAPI_ID}
			buttonText="Logout"
			onLogoutSuccess={onGoogleLogoutSuccess}
			className={styles.googleButton}
		/>
	);
};

export default LogoutGoogle;
