import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { GoogleLogin } from 'react-google-login';
import styles from './googleButtons.module.scss';

const LOGIN_MUTATION = gql`
	mutation LogInMutation {
		loginMutation
	}
`;

const LoginGoogle = ({ setUser }) => {
	const [logInMutation] = useMutation(LOGIN_MUTATION);

	const onGoogleLoginSuccess = async (response) => {
		localStorage.setItem('token', response.tokenId);

		const loginMutationResponse = await logInMutation({
			variables: { token: response.tokenId },
		});

		setUser(loginMutationResponse && loginMutationResponse.data.logInMutation);
	};

	const onGoogleLoginFailure = (response) => {
		console.log(response);
	};

	return (
		<GoogleLogin
			clientId={process.env.REACT_APP_GAPI_ID}
			buttonText="Login"
			onSuccess={onGoogleLoginSuccess}
			onFailure={onGoogleLoginFailure}
			cookiePolicy={'single_host_origin'}
			className={styles.googleButton}
		/>
	);
};

export default LoginGoogle;
