import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { Link, RouteProps } from 'react-router-dom';

import styles from './newsList.module.scss';
import NewsListItem from '../NewsListItem/NewsListItem';
import Header from '../Header/Header';

const GET_NEWS = gql`
	query getNews {
		allNews {
			title
			description
			url
			img
		}
	}
`;

interface Props extends RouteProps {
	name: string;
}

const NewsList = ({ name }: Props) => {
	const { loading, data } = useQuery(GET_NEWS);

	return (
		<>
			<Header name={name} />
			{loading || !data || !data.allNews ? (
				<div>Loading...</div>
			) : (
				<div className={styles.wrapper}>
					<div data-test-id="news-div" className={styles.newslist}>
						{data.allNews.map((article, i) => {
							return (
								<Link to={article.title} key={i}>
									<NewsListItem title={article.title} img={article.img} desc={article.description} />
								</Link>
							);
						})}
					</div>
				</div>
			)}
		</>
	);
};

export default NewsList;
