import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import styles from './article.module.scss';
import Header from '../Header/Header';
import { RouteProps } from 'react-router';

const GET_ARTICLE = gql`
	query getArticle($title: String!) {
		article(newsTitle: $title) {
			title
			description
			url
			img
		}
	}
`;

interface Props extends RouteProps {
	name: string;
}

const Article = (props: Props) => {
	let titleFromUrl = props.location.pathname.substring(1);
	const { loading, data } = useQuery(GET_ARTICLE, { variables: { title: titleFromUrl } });

	return (
		<>
			<Header name={props.name} showBackButton externalURL={data && data.article && data.article.url} />
			<div className={styles.content}>
				{loading || !data || !data.article ? (
					<div>Loading...</div>
				) : !data.article.title ? (
					<div>Article not found.</div>
				) : (
					<div className={styles.articleContent} data-test-id="article-content">
						<h3>{data.article.title}</h3>
						<p>{data.article.description}</p>
						<img src={data.article.img} alt={data.article.title} />
					</div>
				)}
			</div>
		</>
	);
};

export default Article;
