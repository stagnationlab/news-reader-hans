import React from 'react';
import styles from './modal.module.scss';

const Modal = () => {
	return <div className={styles.modal}>Please login to continue.</div>;
};

export default Modal;
