import React from 'react';
import styles from './newsListItem.module.scss';

const NewsListItem = (props) => {
	return (
		<div className={styles.newsitem}>
			<h3>{props.title}</h3>
			<img className={styles.image} alt={props.desc} src={props.img} />
		</div>
	);
};

export default NewsListItem;
