import React, { useState } from 'react';
import styles from './preferences.module.scss';

const Preferences = () => {
	const [science, setScience] = useState(false);
	const [sports, setSports] = useState(false);

	const onFormSubmit = async (event) => {
		alert('Preferences set (not working in this MVP)');
		event.preventDefault();
	};

	return (
		<div className={styles.hm}>
			<form onSubmit={onFormSubmit}>
				<h3>Please select your interests.</h3>
				<div>
					<input type="checkbox" onChange={() => setScience(true)} /> Science:
				</div>
				<div>
					<input type="checkbox" onChange={() => setSports(true)} /> Sports:
				</div>
				<label>
					<input type="submit" />
				</label>
			</form>
		</div>
	);
};

export default Preferences;
