import React from 'react';
import styles from './header.module.scss';
import { Link } from 'react-router-dom';

interface Props {
	externalURL?: string;
	name?: string;
	showBackButton?: boolean;
}

const Header = (props: Props) => {
	const { externalURL, name, showBackButton } = props;
	return (
		<div>
			<div data-test-id="header" className={styles.header}>
				{showBackButton && (
					<Link to="/" className={styles.goBack}>
						<i className="fas fa-chevron-left" />
					</Link>
				)}
				<p>NEWS READER{name && ` - ${name}`}</p>
				{externalURL && (
					<a href={externalURL} className={styles.navigateToUrl}>
						<i className="fas fa-external-link-alt" />
					</a>
				)}
			</div>
		</div>
	);
};

export default Header;
