/// <reference types="Cypress" />
let randomInt0to30 = Math.floor(Math.random() * 31);

describe('News navigation', () => {
	it('goes to authenticated main page', () => {
		cy.visit('/');
		cy.contains('Login').click();
	});

	it('has a list of news (over 30 articles)', () => {
		cy.get('[data-test-id="news-div"]')
			.children()
			.its('length')
			.should('be.gt', 30);
	});

	it('random newsitem has a title and a description', () => {
		cy.get('[data-test-id="news-div"]')
			.children()
			.eq(randomInt0to30)
			.children()
			.children()
			.its('length')
			.should('be', 2);
	});

	it('newsitem has <a> with href', () => {
		cy.get('[data-test-id="news-div"]')
			.children()
			.eq(randomInt0to30)
			.should('have.attr', 'href');
	});

	it('can click on a newsitem to get single article page', () => {
		cy.get('[data-test-id="news-div"]')
			.children()
			.eq(randomInt0to30)
			.click();

		cy.url().should('not.eq', 'http://localhost:3000/');
	});

	it('has both header buttons', () => {
		cy.get('[data-test-id="header"]')
			.wait(500)
			.children()
			.its('length')
			.should('eq', 3);
	});

	it('article component has url for back button and external link button', () => {
		cy.get('[data-test-id="header"]')
			.children()
			.eq(2)
			.should('have.attr', 'href');

		cy.get('[data-test-id="header"]')
			.children()
			.eq(0)
			.should('have.attr', 'href');
	});

	it('single article has all of its content displayed', () => {
		cy.get('[data-test-id="article-content"]')
			.children()
			.eq(0)
			.should('be.visible');
		cy.get('[data-test-id="article-content"]')
			.children()
			.eq(1)
			.should('be.visible');
		cy.get('[data-test-id="article-content"]')
			.children()
			.eq(2)
			.should('be.visible')
			.and('have.attr', 'src');
	});

	it('clicking `back` button from single article takes back to homepage', () => {
		cy.get('[data-test-id="header"]')
			.children()
			.eq(0)
			.click();

		cy.url().should('eq', 'http://localhost:3000/');
	});
});
