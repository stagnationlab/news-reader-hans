/// <reference types="Cypress" />

describe('Log in and log out', () => {
	it('has NEWS READER in header', () => {
		cy.visit('http://localhost:3000');
		cy.contains('NEWS READER');
	});

	it('shows login screen after button press', () => {
		cy.contains('Login').click();
	});

	it('is logged in', () => {
		cy.contains('Logout');
	});

	it('does not have header buttons', () => {
		cy.get('[data-test-id="header"]')
			.children()
			.its('length')
			.should('eq', 1);
	});

	it('logs out', () => {
		cy.contains('Logout').click();
		cy.contains('Login');
		cy.contains('Please login to continue');
	});
});
