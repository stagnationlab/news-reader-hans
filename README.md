# news-reader-hans

## Project setup & running services

- run `git clone https://hanshv@bitbucket.org/stagnationlab/news-reader-hans.git` to clone the repository
- run `yarn install` to install project dependencies
- create local MySQL database `your-database-name`
- create packages/server/config/local-{deployment}.ts with suitable overrides (example below)
- create a .env.local file that overrides .env file in `client` folder. (example below)
  '
- run `yarn start` to start both server & client concurrently in development mode
- run `yarn test` to start both server & client concurrently in test mode
- run `yarn workspace server build` to compile typescript files in `server` folder
- run `yarn production` to start both server & client concurrently in production mode

### local-{deployment} configuration file example

    export default {
        database: {
            databaseName: "name of your database",
            password: "password for your database",
            port: port for the database,
            GAPI_ID: "GAPI_ID"
        },
        server: {
            port: yourserverport
        },
        origin: 'clientdomain:clientport'
    };

### .env.local file example

    REACT_APP_GAPI_ID=yourgoogleclientid
    REACT_APP_DEVELOPMENT_URL=http://localhost:5000
    REACT_APP_PRODUCTION_URL=http://localhost:5001
    REACT_APP_TEST_URL=http://localhost:5002

Release 1.0.0: Able to login and fetch regular news.  
Release 1.0.2: News are rendered from database. Extended article component is added.
Release 1.0.3: Functionality is almost done, app is ready for testing.
Release 1.0.3: Tests added to the application.
